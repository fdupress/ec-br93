require import AllCore Distr Real DBool List FSet SmtMap.

pragma +implicits.
pragma -oldip.

(* A type for nonces, with a (sub-)distribution over it *)
type rand.

op drand: rand distr.
axiom drand_ll: is_lossless drand.

(* A type for plaintexts with a (sub-)distribution and a + operator (infix) *)
type ptxt.

op dptxt : ptxt distr.
axiom dptxt_ll: is_lossless dptxt.
axiom dptxt_funi: is_funiform dptxt.

op (+) : ptxt -> ptxt -> ptxt.
axiom addpK x y: x + y + y = x.

(* A trapdoor permutations over type rand *)
type skey, pkey.

op dkeys : (skey * pkey) distr.
axiom dkeys_ll: is_lossless dkeys.

op f    : pkey -> rand -> rand.
op finv : skey -> rand -> rand.

axiom fK sk pk x:
  (sk,pk) \in dkeys =>
  finv sk (f pk x) = x.

module type TP = {
  proc keygen() : skey * pkey
  proc p(_ : pkey * rand) : rand
  proc pinv(sk : skey, x : rand) : rand
}.

module (P : TP) = {
  proc keygen() : skey * pkey = {
    var kp;

    kp <$ dkeys;
    return kp;
  }

  proc p(pk : pkey, x : rand) : rand = {
    return f pk x;
  }

  proc pinv(sk : skey, y : rand) : rand = {
    return finv sk y;
  }
}.

module type TPInv = {
  proc invert(_ : pkey * rand) : rand
}.

module OWTP (A : TPInv) (P : TP) = {
  proc main() = {
    var sk, pk, x, y, x';

    (sk,pk) <@ P.keygen();
    x       <$ drand;
    y       <@ P.p(pk, x);
    x'      <@ A.invert(pk, y);
    return x = x';
  }
}.

(* An interface for random oracles from rand to ptxt *)
module type FRO = {
  proc init() : unit
  proc o(_ : rand) : ptxt
}.

module type RO = {
  proc o(_ : rand) : ptxt
}.

module (H : RO) = {
  var m : (rand, ptxt) fmap

  proc init() : unit = {
    m <- empty;
  }

  proc o(x : rand) : ptxt = {
    var r;

    r <$ dptxt;
    if (x \notin m) {
      m.[x] <- r;
    }
    return oget m.[x];
  }
}.

lemma Ho_ll: islossless H.o.
proof. by proc; auto=> />; rewrite dptxt_ll. qed.

(** IND-CPA Asymmetric Encryption schemes with ctxt = rand * ptxt,
    and a random oracle from rand to ptxt **)
type ctxt = rand * ptxt.

module type CPA_Adv (H : RO) = {
  proc choose(_ : pkey) : ptxt * ptxt
  proc guess(_ : ctxt) : bool
}.

module type AEScheme (H : RO) = {
  proc keygen(): skey * pkey
  proc enc(_ : pkey * ptxt) : ctxt
  proc dec(_ : skey * ctxt) : ptxt
}.

module INDCPA (A : CPA_Adv) (S : AEScheme) (H : FRO) = {
  proc main() = {
    var sk, pk, m0, m1, b, c, b';

    H.init();
    (sk,pk) <@ S(H).keygen();
    (m0,m1) <@ A(H).choose(pk);
    b       <$ {0,1};
    c       <@ S(H).enc(pk,if b then m1 else m0);
    b'      <@ A(H).guess(c);
    return b = b';
  }
}.

(** Bellare-Rogaway's 1993 scheme **)
module BR93 (P : TP) (H : RO) = {
  proc keygen(): skey * pkey = {
    var k : skey * pkey;

    k <@ P.keygen();
    return k;
  }

  proc enc(pk: pkey, m: ptxt): rand * ptxt = {
    var r, s: rand;
    var h : ptxt;

    r <$ drand;
    s <@ P.p(pk, r);
    h <@ H.o(r);
    return (s, h + m);
  }

  proc dec(sk: skey, c: rand * ptxt): ptxt = {
    var s, g, r, h;

    (s, g) <- c;
    r      <@ P.pinv(sk, s);
    h      <@ H.o(r);
    return g + h;
  }
}.

(* Security Proof *)
module (I (A : CPA_Adv) : TPInv) = {
  proc invert(pk : pkey, y : rand) = {
    var h, dH, z, fd;

          H.init();
          A(H).choose(pk);
    h  <$ dptxt;
          A(H).guess(y, h);
    (* Search through the random oracle inputs *)
    dH <- fdom H.m;
    z  <- witness;
    fd <- false;
    while (dH <> fset0 /\ !fd) {
      z  <- pick dH;
      dH <- dH `\` fset1 z;
      if (f pk z = y) {
        fd <- true;
      }
    }
    return z;
  }
}.

section SecProof.
declare module A : CPA_Adv { H }.
axiom Achoose_ll (H <: RO { A }): islossless H.o => islossless A(H).choose.
axiom Aguess_ll  (H <: RO { A }): islossless H.o => islossless A(H).guess.

local module Game0 = {
  proc main() = {
    var sk, pk, m0, m1, b, m, r, s, h, c, b';

    H.init();
    (sk,pk) <@ P.keygen();
    (m0,m1) <@ A(H).choose(pk);
    b       <$ {0,1};
    m       <- if b then m1 else m0;
    r       <$ drand;
    s       <@ P.p(pk,r);
    h       <@ H.o(r);
    c       <- (s, h + m);
    b'      <@ A(H).guess(c);
    return b = b';
  }
}.

local lemma INDCPA_Game0 &m:
  Pr[INDCPA(A,BR93(P),H).main() @ &m: res] = Pr[Game0.main() @ &m: res].
proof.
byequiv (: ={glob A} ==> ={res})=> //.
by proc; inline BR93(P,H).enc BR93(P,H).keygen P.keygen; sim.
qed.

local module Game1 = {
  var r : rand

  proc main() = {
    var sk, pk, m0, m1, b, m, s, h, c, b';

    H.init();
    (sk,pk) <@ P.keygen();
    (m0,m1) <@ A(H).choose(pk);
    b       <$ {0,1};
    m       <- if b then m1 else m0;
    r       <$ drand;
    s       <@ P.p(pk,r);
    h       <$ dptxt;
    c       <- (s, h + m);
    b'      <@ A(H).guess(c);
    return b = b';
  }
}.

local lemma Game0_Game1 &m:
     Pr[Game0.main() @ &m: res]
  <=   Pr[Game1.main() @ &m: res]
     + Pr[Game1.main() @ &m: Game1.r \in H.m].
proof.
byequiv (: ={glob A} ==> _)=> //=.
proc.
seq  7  7: (={glob A, glob H, b, m, s, sk, pk} /\ r{1} = Game1.r{2}).
+ by sim.
call (: Game1.r \in H.m, eq_except (pred1 Game1.r{2}) H.m{1} H.m{2}).
+ exact/Aguess_ll.
+ proc; auto=> /> &1 &2 r_notin_H eqe_H1_H2 r _; case: (x{2} = Game1.r{2}).
  + by move=> ->>; rewrite !domE r_notin_H /= !get_set_sameE !oget_some.
  move=> x_neq_r; have /eq_exceptP /(_ _ x_neq_r) H1x_H2x:= eqe_H1_H2.
  move: r_notin_H; rewrite !domE /= H1x_H2x=> />.
  by rewrite !get_set_sameE=> /= _ _ _; exact/eq_except_set_eq.
+ by move=> _ _; exact/Ho_ll.
+ move=> _; conseq Ho_ll (: _ ==> _)=> //=.
  by proc; auto=> /> &hr + r _ _; rewrite mem_set=> ->.
wp; conseq (: _ ==>
              Game1.r{2} \notin H.m{2} =>
              (   ={glob A, s, h, m, b}
               /\ eq_except (pred1 Game1.r{2}) H.m{1} H.m{2}))=> />.
+ move=> &2 m hL hR h; split=> [/h [] -> -> //| _].
  by move=> {hL hR h} b' aL hL aR hR h /h [] ->.
inline H.o; auto=> /> &2 r _.
by rewrite get_set_sameE oget_some eq_except_setl.
qed.

local module Game2 = {
  var r : rand

  proc main() = {
    var sk, pk, m0, m1, b, s, h, c, b';

    H.init();
    (sk,pk) <@ P.keygen();
    (m0,m1) <@ A(H).choose(pk);
    b       <$ {0,1};
    r       <$ drand;
    s       <@ P.p(pk,r);
    h       <$ dptxt;
    c       <- (s, h);
    b'      <@ A(H).guess(c);
    return b = b';
  }
}.

local equiv Game1_Game2_eq:
  Game1.main ~ Game2.main:
    ={glob A} ==> ={glob H, res} /\ ={r}(Game1,Game2).
proof.
proc; sim.
wp; rnd (fun h=> h + m{1}).
conseq (: _ ==> ={glob A, glob H, b, s} /\ ={r}(Game1,Game2))=> />.
+ by move=> />; smt(addpK dptxt_funi).
by sim.
qed.

local lemma Game1_Game2_res &m:
  Pr[Game1.main() @ &m: res] = Pr[Game2.main() @ &m: res].
proof. by byequiv Game1_Game2_eq. qed.

local lemma Game1_Game2_r_in_H &m:
  Pr[Game1.main() @ &m: Game1.r \in H.m] = Pr[Game2.main() @ &m: Game2.r \in H.m].
proof. by byequiv Game1_Game2_eq. qed.

local lemma Game2_res &m:
  Pr[Game2.main() @ &m: res] = 1%r/2%r.
proof.
byphoare=> //. proc. swap 4 5. rnd (pred1 b').
conseq (: _ ==> true)=> />.
+ by move=> b'; rewrite dbool1E.
inline P.keygen P.p H.init; call (Aguess_ll H Ho_ll).
auto; call (Achoose_ll H Ho_ll).
by auto=> />; rewrite dkeys_ll drand_ll dptxt_ll.
qed.

local module Game3 = {
  proc main() = {
    var sk, pk, x, y, h, z;

    (sk,pk) <@ P.keygen();
               H.init();
               A(H).choose(pk);
    x       <$ drand;
    y       <@ P.p(pk, x);
    h       <$ dptxt;
               A(H).guess(y, h);
    z       <- nth
                 witness
                 (elems (fdom H.m))
                 (find (fun z=> f pk z = y) (elems (fdom H.m)));
    return x = z;
  }
}.

local lemma Game2_Game3 &m:
  Pr[Game2.main() @ &m: Game2.r \in H.m] <= Pr[Game3.main() @ &m: res].
proof.
byequiv (: ={glob A} ==> _)=> //; proc.
inline *; wp; call (: ={glob H}); first by sim.
auto; call (: ={glob H}); first by sim.
auto; rewrite dbool_ll=> /> [] sk pk /= valid_keys _ _ x _ _ _ m.
rewrite -mem_fdom memE=> x_in_m.
have ->: (fun z=> f pk z = f pk x) = pred1 x.
+ apply/pred_ext=> z /= @/pred1; split=> //.
  by rewrite -{2}(@fK _ _ z valid_keys)=> ->; rewrite (fK valid_keys).
by rewrite eq_sym; exact/(@nth_find _ (pred1 x))/has_pred1.
qed.

local lemma Game3_OWTP &m:
  Pr[Game3.main() @ &m: res] = Pr[OWTP(I(A),P).main() @ &m: res].
proof.
byequiv (: ={glob A} ==> _)=> //=; proc.
inline I(A).invert. swap{1} [2..3] 2.
seq  7  9: (={glob H, x} /\ pk{1} = pk0{2}); first by sim.
admitted.

lemma Security &m:
  Pr[INDCPA(A,BR93(P),H).main() @ &m: res] - 1%r/2%r <=
  Pr[OWTP(I(A),P).main() @ &m: res].
proof.
smt(INDCPA_Game0 Game0_Game1 Game1_Game2_res Game1_Game2_r_in_H Game2_res Game2_Game3 Game3_OWTP).
qed.
end section SecProof.
